import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getAuth, signInAnonymously } from "firebase/auth";
import { getStorage, ref, listAll, getDownloadURL } from "firebase/storage";
import {
  getFirestore,
  doc,
  addDoc,
  getDoc,
  getDocs,
  writeBatch,
  where,
  orderBy,
  query,
  FieldValue,
  serverTimestamp,
  collection,
} from "firebase/firestore";
import { async } from "@firebase/util";

const firebaseConfig = {
  apiKey: "AIzaSyDqWiY7JLwQin_1lKYaUKb7LD_kh_-vEU4",
  authDomain: "dddd-275917.firebaseapp.com",
  projectId: "dddd-275917",
  storageBucket: "dddd-275917.appspot.com",
  messagingSenderId: "1084723802576",
  appId: "1:1084723802576:web:a7e4f3c2143af62275c9d5",
  measurementId: "G-JSZQYV3R4F",
};

// Create a reference under which you want to list

export const firebaseapp = initializeApp(firebaseConfig);
export const db = getFirestore(firebaseapp);
const storage = getStorage();
const listRef = ref(storage, "/");
const auth = getAuth(firebaseapp);
export async function loginAnonymously() {
  try {
    await signInAnonymously(auth);
    return true;
  } catch (error) {
    console.log(error);
  }
}

const fileLink = [
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/1.jpg?alt=media&token=28298d24-68e7-4ef6-beb3-4a4c9a5252d6",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/10.jpg?alt=media&token=63c1fd8f-468e-411e-95a7-f27674634ac8",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/11.jpg?alt=media&token=db139327-fdbd-4726-912c-be49e9a5f972",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/12.jpg?alt=media&token=d5c50ea5-a329-4872-bb3c-e4d4e13daf9d",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/13.jpg?alt=media&token=bdadc450-aa42-4578-98b2-7973ff1d4a67",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/14.jpg?alt=media&token=460e9dee-df0e-4401-aec7-bc98343ab541",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/15.jpg?alt=media&token=403bc617-6943-4757-ae80-8d627401971f",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/2.jpg?alt=media&token=df9c85cf-dffe-4cd0-9a78-9eac4c5f4618",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/3.jpg?alt=media&token=b1e11ec6-4200-45a0-83c0-feaa2c6a00f2",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/4.jpg?alt=media&token=97c7641e-537f-462c-8387-d4af18a4c4eb",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/5.jpg?alt=media&token=385b3036-8be9-45e1-a37f-8c14e7a94aaf",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/6.jpg?alt=media&token=78aaa9d7-a264-4203-a32f-9d210a25daf2",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/7.jpg?alt=media&token=500f71f3-1a18-4f98-a7e5-284e6c74ec48",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/8.jpg?alt=media&token=1d851883-8786-4771-8aa9-b1879551ea5d",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/9.jpg?alt=media&token=176bb714-71dd-48e8-bdf1-10ab61a733f7",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/DSCF0209%20copy.jpg?alt=media&token=c7e07680-5450-4084-99d6-41f55fae2b3d",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/DSCF0251%20copy.jpg?alt=media&token=2cc71830-aeb9-4623-8bb9-0aae1663ac4d",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/DSCF0286%20copy.jpg?alt=media&token=ca3e15a8-bec3-4099-ba46-c2d45325bbd5",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/DSCF0290%20copy.jpg?alt=media&token=5d71c82b-c333-4f2a-8cea-fc55bee02b33",
  "https://firebasestorage.googleapis.com/v0/b/dddd-275917.appspot.com/o/DSCF0324%20copy.jpg?alt=media&token=9b3a280f-4308-4ae0-9b54-fbc16e79c21a",
];
export async function getCongrats() {
  const congratDoc = collection(db, "congrats");
  const q = query(congratDoc, orderBy("createdAt"));
  const congratSnapshot = await getDocs(congratDoc);
  let congrats = [];

  for (let index = 0; index < congratSnapshot.docs.length; index++) {
    const element = congratSnapshot.docs[index];
    congrats.push(element.data());
  }

  congrats.sort(function (a, b) {
    return new Date(b.createdAt) - new Date(a.createdAt);
  });
  return congrats;
}
export async function showAllItem() {
  var urlList = [];
  // Find all the prefixes and items.
  var itemRef = await listAll(listRef);
  for (let index = 0; index < itemRef.items.length; index++) {
    const element = itemRef.items[index];
    console.log(element);
    let url = await getDownloadURL(ref(storage, element.fullPath));
    urlList.push(url);
  }
  return urlList;
}

export async function generatePhotos() {
  try {
    for (let index = 0; index < fileLink.length; index++) {
      const element = fileLink[index];
      console.log(element);
      const docRef = await addDoc(collection(db, "photos"), {
        name: "",
        url: element,
      });
    }
  } catch (error) {
    console.log(error);
  }
}
export async function getPhotos() {
  const photoDoc = collection(db, "photos");
  const photoSnapShot = await getDocs(photoDoc);
  let photos = [];
  photoSnapShot.forEach((doc) => {
    photos.push(doc.data());
  });
  return photos;
}
export async function getUploadStatus(name) {
  const congratCollection = collection(db, "congrats");
  const q = query(congratCollection, where("name", "==", name));
  const querySnapshot = await getDocs(q);
  let existedData = [];
  querySnapshot.forEach((doc) => {
    existedData.push(doc.data());
  });
  if (existedData.length > 0) {
    return true;
  } else {
    return false;
  }
}
export async function getAttendingStatus(name) {
  const congratCollection = collection(db, "attending");
  const q = query(congratCollection, where("name", "==", name));
  const querySnapshot = await getDocs(q);
  let existedData = [];
  querySnapshot.forEach((doc) => {
    existedData.push(doc.data());
  });

  if (existedData.length > 0) {
    return true;
  } else {
    return false;
  }
}

export async function addGoingStatus(name, going) {
  try {
    const docRef = await addDoc(collection(db, "attending"), {
      name: name,
      going: going,
    });
  } catch (e) {
    console.error("Error adding document: ", e);
  }
}
export async function addCongrats(name, congrat, going) {
  try {
    // Create Data
    const timestamp = Date();
    console.log(timestamp);
    const docRef = await addDoc(collection(db, "congrats"), {
      name: name,
      congrat: congrat,
      going: going,
      createdAt: timestamp,
    });
  } catch (e) {
    console.error("Error adding document: ", e);
  }
}
const analytics = getAnalytics(firebaseapp);
